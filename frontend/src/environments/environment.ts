export const environment = {
    apiUrl: 'http://localhost:7727/lab4/api',
    loginPath: '/auth/login',
    registerPath: '/auth/register',
    shotsNew: '/shots/new',
    shotsList: '/shots/list',
    shotsClear: '/shots/clear',
};
