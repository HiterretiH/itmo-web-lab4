export const environment = {
    apiUrl: '/api',
    loginPath: '/auth/login',
    registerPath: '/auth/register',
    shotsNew: '/shots/new',
    shotsList: '/shots/list',
    shotsClear: '/shots/clear',
};
