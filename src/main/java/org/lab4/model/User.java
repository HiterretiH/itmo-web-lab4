package org.lab4.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString
@Table(name = "app_user")
public class User implements Serializable {
    @Id
    @GeneratedValue
    private long id;
    @Column(nullable = false, unique = true)
    @Pattern(regexp = "[a-zA-Z0-9_]{6,20}")
    private String username;
    @Column(nullable = false)
    private String passwordHash;
    @Column(nullable = false)
    private String salt;

    public User() {}
}
