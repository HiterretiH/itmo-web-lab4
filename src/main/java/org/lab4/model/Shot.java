package org.lab4.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString
public class Shot implements Serializable {
    @Id
    @GeneratedValue
    private long id;
    private double x;
    private double y;
    private double r;
    private boolean hit;
    private String time;
    @JoinColumn(nullable = false)
    @ManyToOne
    private User user;

    public Shot() {}
}
