package org.lab4;

import jakarta.ejb.EJB;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.lab4.json.Credentials;
import org.lab4.json.UserToken;
import org.lab4.model.User;
import org.lab4.repository.UserRepository;
import org.lab4.security.PasswordManager;
import org.lab4.security.TokenService;

import java.util.Date;

@Path("/auth")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthResource {
    @EJB
    UserRepository userRepository;
    private static final long tokenDuration = 24 * 60 * 60 * 1000;

    @POST
    @Path("/register")
    public Response register(@Valid @NotNull Credentials credentials) {
        if (userRepository.getUserByUsername(credentials.getUsername()) != null) {
            return Response.status(Response.Status.CONFLICT)
                    .entity("User with username " + credentials.getUsername() + " already exists.")
                    .type(MediaType.TEXT_PLAIN)
                    .build();
        }

        Date expirationDate = new Date(System.currentTimeMillis() + tokenDuration);
        String token = TokenService.generateToken(credentials.getUsername(), expirationDate);

        User user = new User();
        user.setUsername(credentials.getUsername());
        PasswordManager.setPassword(user, credentials.getPassword());

        userRepository.saveUser(user);

        return Response.ok(new UserToken(token, expirationDate.toInstant().toEpochMilli())).build();
    }

    @POST
    @Path("/login")
    public Response login(@Valid @NotNull Credentials credentials) {
        User user = userRepository.getUserByUsername(credentials.getUsername());

        if (user == null || !PasswordManager.validatePassword(user, credentials.getPassword())) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .entity("Invalid username or password.")
                    .type(MediaType.TEXT_PLAIN)
                    .build();
        }

        Date expirationDate = new Date(System.currentTimeMillis() + tokenDuration);
        String token = TokenService.generateToken(credentials.getUsername(), expirationDate);

        return Response.ok(new UserToken(token, expirationDate.toInstant().toEpochMilli())).build();
    }
}
