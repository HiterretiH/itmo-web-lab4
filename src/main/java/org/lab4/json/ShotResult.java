package org.lab4.json;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShotResult {
    private double x;
    private double y;
    private double r;
    private boolean hit;
    private String time;
}
