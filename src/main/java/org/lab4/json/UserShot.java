package org.lab4.json;

import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserShot {
    @NotNull(message = "X coordinate cannot be null")
    @DecimalMin(value = "-3.0", message = "X coordinate must be greater than or equal to -3.0")
    @DecimalMax(value = "5.0", message = "X coordinate must be less than or equal to 5.0")
    private double x;
    @NotNull(message = "Y coordinate cannot be null")
    @DecimalMin(value = "-3.0", message = "Y coordinate must be greater than or equal to -3.0")
    @DecimalMax(value = "5.0", message = "Y coordinate must be less than or equal to 5.0")
    private double y;
    @NotNull(message = "R radius cannot be null")
    @DecimalMin(value = "0.0", inclusive = false, message = "R radius must be greater than 0.0")
    @DecimalMax(value = "5.0", message = "R radius must be less than or equal to 5.0")
    private double r;
}
