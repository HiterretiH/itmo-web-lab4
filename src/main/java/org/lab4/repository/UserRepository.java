package org.lab4.repository;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;
import lombok.extern.jbosslog.JBossLog;
import org.lab4.model.User;

@Stateless
@JBossLog
public class UserRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void saveUser(User user) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            log.error("Error saving user " + user, e);
        }
    }

    public User getUserByUsername(String username) {
        TypedQuery<User> query = entityManager
                .createQuery("SELECT u FROM User u WHERE u.username = :username", User.class)
                .setParameter("username", username);
        return query.getResultStream().findAny().orElse(null);
    }
}
