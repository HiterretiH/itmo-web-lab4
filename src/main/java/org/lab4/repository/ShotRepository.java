package org.lab4.repository;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import lombok.extern.jbosslog.JBossLog;
import org.lab4.model.Shot;
import org.lab4.model.User;

import java.util.List;

@Stateless
@JBossLog
public class ShotRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void saveShot(Shot shot) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(shot);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            log.error("Error saving shot " + shot, e);
        }
    }

    public List<Shot> loadShotsByUser(User user) {
        return entityManager.createQuery("SELECT s FROM Shot s WHERE s.user = :user", Shot.class)
                .setParameter("user", user)
                .getResultList();
    }

    @Transactional
    public void deleteShotsByUser(User user) {
        try {
            entityManager.getTransaction().begin();
            entityManager.createQuery("DELETE FROM Shot s WHERE s.user = :user")
                    .setParameter("user", user)
                    .executeUpdate();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            log.error("Error deleting shots for user " + user, e);
        }
    }
}
