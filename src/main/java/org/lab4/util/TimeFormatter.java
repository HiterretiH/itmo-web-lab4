package org.lab4.util;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeFormatter {
    public static String getFormattedTime() {
        return LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }
}
