package org.lab4.util;

import org.lab4.json.ShotResult;

public class CollisionChecker {
    public static boolean checkCollision(ShotResult shot) {
        double x = shot.getX();
        double y = shot.getY();
        double r = shot.getR();

        return (x >= 0 && y >=0 && x * x + y * y <= r * r) ||
                (x <=0 && y >= 0 && x >= -r * 0.5 && y <= r) ||
                (x <= 0 && y <= 0 && y >= -x * 0.5 - r * 0.5);
    }
}
