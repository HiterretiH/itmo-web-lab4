package org.lab4.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.lab4.exceptions.TokenExpiredException;
import org.lab4.exceptions.TokenIsInvalidException;

import javax.crypto.SecretKey;
import java.util.Date;

public class TokenService {
    private static final SecretKey secretKey = Keys.hmacShaKeyFor("when you see it longer longer key".getBytes());

    public static String generateToken(String username, Date expiration) {
        return Jwts.builder()
                .issuer(username)
                .expiration(expiration)
                .signWith(secretKey)
                .compact();
    }

    public static String validateToken(String token) throws TokenIsInvalidException, TokenExpiredException {
        try {
            Claims claims = Jwts.parser()
                    .verifyWith(secretKey)
                    .build()
                    .parseSignedClaims(token)
                    .getPayload();

            if (claims.getExpiration().before(new java.util.Date())) {
                throw new TokenExpiredException();
            }

            return claims.getIssuer();
        }
        catch (JwtException e) {
            throw new TokenIsInvalidException();
        }
    }
}
