package org.lab4.security;

import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.SecurityContext;
import jakarta.ws.rs.ext.Provider;
import org.lab4.exceptions.TokenExpiredException;
import org.lab4.exceptions.TokenIsInvalidException;

import java.security.Principal;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {
    @Override
    public void filter(ContainerRequestContext requestContext) {
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            requestContext.abortWith(formResponse("Authorization header must be provided"));
            return;
        }

        String token = authorizationHeader.substring("Bearer".length()).trim();

        try {
            String username = TokenService.validateToken(token);

            requestContext.setSecurityContext(
                    createSecurityContext(username)
            );
        }
        catch (TokenExpiredException e) {
            requestContext.abortWith(formResponse("Token expired"));
        }
        catch (TokenIsInvalidException e) {
            requestContext.abortWith(formResponse("Token is invalid"));
        }
    }

    private SecurityContext createSecurityContext(String username) {
        return new SecurityContext() {
            @Override
            public Principal getUserPrincipal() {
                return () -> username;
            }

            @Override
            public boolean isUserInRole(String s) {
                return true;
            }

            @Override
            public boolean isSecure() {
                return false;
            }

            @Override
            public String getAuthenticationScheme() {
                return null;
            }
        };
    }

    private Response formResponse(String message) {
        return Response.status(Response.Status.UNAUTHORIZED)
                        .entity(message)
                        .type(MediaType.TEXT_PLAIN)
                        .build();
    }
}
