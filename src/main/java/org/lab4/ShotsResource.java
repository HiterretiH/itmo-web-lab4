package org.lab4;

import jakarta.ejb.EJB;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.SecurityContext;
import org.lab4.json.ShotResult;
import org.lab4.json.UserShot;
import org.lab4.mappers.ShotMapper;
import org.lab4.model.Shot;
import org.lab4.model.User;
import org.lab4.repository.ShotRepository;
import org.lab4.repository.UserRepository;
import org.lab4.security.Secured;
import org.lab4.util.CollisionChecker;
import org.lab4.util.TimeFormatter;

@Path("/shots")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ShotsResource {
    @EJB
    private ShotRepository shotRepository;
    @EJB
    private UserRepository userRepository;
    @Context
    private SecurityContext securityContext;
    @Inject
    ShotMapper shotMapper;

    @GET
    @Secured
    @Path("/list")
    public Response getAll() {
        String username = securityContext.getUserPrincipal().getName();
        User user = userRepository.getUserByUsername(username);

        if (user == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("User with username " + username + " is not found")
                    .type(MediaType.TEXT_PLAIN)
                    .build();
        }

        return Response.ok(
                shotMapper.toShotResultList(
                        shotRepository.loadShotsByUser(user)
                )
        ).build();
    }

    @POST
    @Secured
    @Path("/new")
    public Response createNew(@NotNull @Valid UserShot shot) {
        String username = securityContext.getUserPrincipal().getName();
        User user = userRepository.getUserByUsername(username);

        if (user == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("User with username " + username + " is not found")
                    .type(MediaType.TEXT_PLAIN)
                    .build();
        }

        ShotResult newShot = shotMapper.toShotResult(shot);

        newShot.setHit(CollisionChecker.checkCollision(newShot));
        newShot.setTime(TimeFormatter.getFormattedTime());

        Shot entity = shotMapper.toShot(newShot);
        entity.setUser(user);
        shotRepository.saveShot(entity);

        return Response.ok(newShot).build();
    }

    @POST
    @Secured
    @Path("/clear")
    public Response clear() {
        String username = securityContext.getUserPrincipal().getName();
        User user = userRepository.getUserByUsername(username);

        if (user == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("User with username " + username + " is not found")
                    .type(MediaType.TEXT_PLAIN)
                    .build();
        }

        shotRepository.deleteShotsByUser(user);

        return Response.ok().build();
    }
}
