package org.lab4.mappers;

import org.lab4.json.ShotResult;
import org.lab4.json.UserShot;
import org.lab4.model.Shot;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.CDI)
public interface ShotMapper {
    @Mapping(target = "time", ignore = true)
    @Mapping(target = "hit", ignore = true)
    ShotResult toShotResult(UserShot userShot);

    @Mapping(target = "user", ignore = true)
    @Mapping(target = "id", ignore = true)
    Shot toShot(ShotResult shotResult);

    List<ShotResult> toShotResultList(List<Shot> shots);
}
